package net.bertag.immutablesdemo.part1;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Parameter;

@Immutable
public abstract class EImmutables implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Parameter
  public abstract String id();
  
  public abstract Optional<String> description();

  public abstract List<Integer> counts();  

}
