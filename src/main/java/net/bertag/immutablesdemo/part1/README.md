## Introduction

Let's talk about how we manage value objects in Java.  These are objects whose primary purpose is to
model data and provide methods for working with that data.  Many of these are also what we might
refer to as proper "Value-based Classes", defined by Oracle itself
(https://docs.oracle.com/javase/8/docs/api/java/lang/doc-files/ValueBased.html) as classes which:

1. Are final and immutable (though may contain references to mutable objects);
2. Have implementations of equals, hashCode, and toString which are computed solely from the
   instance's state and not from its identity or the state of any other object or variable;
3. Make no use of identity-sensitive operations such as reference equality (==) between instances,
   identity hash code of instances, or synchronization on an instances's intrinsic lock;
4. Are considered equal solely based on equals(), not based on reference equality (==);
5. Do not have accessible constructors, but are instead instantiated through factory methods which
   make no commitment as to the identity of returned instances;
6. Are freely substitutable when equal, meaning that interchanging any two instances x and y that are
   equal according to equals() in any computation or method invocation should produce no visible
   change in behavior.

But let's be honest, we often use classes as value objects without strictly following these
guidelines, and that's really probably ok in most contexts, and in the process we make a lot of
assumptions about them.

Consider for example a simple Java Bean, **ATraditionalBean**.  This class has 3 simple attributes,
accompanied by boilerplate accessors, but on its own tells us nothing about how these attributes are
to be used. For example, it is completely valid, and indeed must be expected following construction,
that all attributes are null, and can be reset to null at anytime.  A responsible developer must
therefore meticulously perform nullness-checks, or offer some guarantee-by-convention that certain
fields will always be initialized immediately following construction.

It is worth noting that while Javadoc could provide context to the developer about how the class
_should_ be used, no amount of documentation changes the fact that the _default_ state of the object
is inherently an invalid state.

We could, of course, tweak this traditional JavaBean to enforce certain constraints, which we do
when defining **BStrictBean**.  If `id` represents a "required" field, for example, we require the
use of a public constructor that defines a non-null `id` at construction time (but note that doing
so breaks the JavaBean contract; many JavaBean-based libraries will accept a private "default"
constructor, but not all).  We also supply a sane default for the `counts` list.

Since we have already necessarily broken the JavaBean contract by relying upon a non-default
constructor to provide some guarantees of non-nullness, we could go a step further and adopt many of
the requirements of Oracle's formal definition of a "Value-based class", which we do in
**CValueObject**.  Note that by simply declaring **CValueObject** to be both final and immutable, we
satisfy all of Oracle's requirements except for #5 (no accessible constructors).

In order to create an immutable object however, we must necessarily define a constructor that
accepts values for (or provides defaults for) **all** class members.  Note that in a simple class
such as this, with only a few members, this is rather trivial, but on a large object containing
dozens of attributes, this becomes overbearingly difficult to maintain (a problem that
**BStrictBean** also suffers from should the list of "required" members become large).  Therefore,
we could alternatively employ the builder pattern to reduce the complexity of the constructor and
increase readability of the resulting application code, which we do in **DBuiltValueObject**.

It is worth noting that Google has identified that simple structs such as we've defined here --
simple containers containing no additional business logic -- impose a "fixed cost" amount of code 
(approx. 30 lines) for an object containing a single member.  Each additional member adds an
additional 10 lines of code.  And this is the best-case scenario.  Because **ATraditionalBean**
offers no protections against invalid state, there are steep, but hidden costs associated with using
the class downstream.  And **DBuiltValueObject** adds additional cost by requiring each member to be
represented in both the final object and the builder.  This is particularly concerning since **A**
and **D** are probably the most scaleable patterns to use for large objects, where the amount of
code involved is even likely to be an issue.

The Java Immutables library (https://immutables.github.io) offers a solution to these issues by
guaranteeing code safety and doing so in a way that is scaleable and concise.  Rather than define a
class outright, the developer instead defines _**The Contract**_, either as an abstract class or as
an interface.  The Immutables library then uses annotation processing to generate the corresponding
implementation of that contract at compile time.  This implementation strictly follows Oracle's
guidelines for a "Value-based Class".

The use of this library is demonstrated in the **EImmutables** class, and the **EImmutablesTest**
class is provided to offer a quick demo of how it works from the perspective of the downstream user.
Note that compared to any of the previous classes, the **EImmutables** class is incredibly terse,
while using it (and even "modifying" it using copy constructors) is no more verbose than using a
traditional Java Bean (see **ATraditionalBeanTest**).

There are tradeoffs for this conciseness of course.  With the Immutables library, these tradeoffs
are primarily centered around the relative verboseness of the generated implementation of
_**The Contract**_, which for this simple example approaches 400 lines, half of which are spent on
the builder and some safeness guarantees regarding the included list.  This tradeoff is mitigated
by the fact that this generated code is "out of sight, out of mind".  The developer is not
responsible for maintaining nor testing it.

But importantly, performance is one area where we do not have to make tradeoffs (with one important
exception).  Using the generated `with___` methods to copy-construct new instances of
**EImmutables** is equally as fast as modifying **ATraditionalBean** using traditional `set___`
methods, even when the classes are updated hundreds of millions of times.  The noted exception
involves updating the list contained in both classes.  In this case, using traditional list mutators
is consistently faster than relying on **Stream** concatenation or **Collection** copy constructors
to rebuild the immutable list, particularly over large numbers of iterations.