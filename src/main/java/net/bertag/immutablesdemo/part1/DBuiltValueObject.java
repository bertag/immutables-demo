package net.bertag.immutablesdemo.part1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DBuiltValueObject implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private final String id;
  private final String description;
  private final List<Integer> counts;
  
  private DBuiltValueObject(Builder builder) {
    this.id = builder.id;
    this.description = builder.description;
    this.counts = builder.counts;
  }
  
  public String getId() {
    return id;
  }
  
  public String getDescription() {
    return description;
  }
  
  public List<Integer> getCounts() {
    return counts;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id, description, counts);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    
    DBuiltValueObject other = (DBuiltValueObject) obj;
    return Objects.equals(id, other.id)
        && Objects.equals(description, other.description)
        && Objects.equals(counts, other.counts);
  }
  
  public static class Builder {
    
    private String id;
    private String description;
    private List<Integer> counts = new ArrayList<>();
    
    public Builder(String id) {
      this.id = Objects.requireNonNull(id);
    }
    
    public Builder description(String description) {
      this.description = description;
      return this;
    }
    
    public Builder counts(List<Integer> counts) {
      this.counts = counts;
      return this;
    }
    
    public DBuiltValueObject build() {
      return new DBuiltValueObject(this);
    }
    
  }
  
}
