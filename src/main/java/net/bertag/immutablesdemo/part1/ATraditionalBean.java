package net.bertag.immutablesdemo.part1;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class ATraditionalBean implements Serializable {
  
  private static final long serialVersionUID = 1L;

  private String id;
  private String description;
  private List<Integer> counts;
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public List<Integer> getCounts() {
    return counts;
  }
  
  public void setCounts(List<Integer> counts) {
    this.counts = counts;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id, description, counts);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    
    ATraditionalBean other = (ATraditionalBean) obj;
    return Objects.equals(id, other.id)
        && Objects.equals(description, other.description)
        && Objects.equals(counts, other.counts);
  }
  
  @Override
  public String toString() {
    return "ATraditionalBean [id=" + id + ", description=" + description + ", counts=" + counts
        + "]";
  }
  
}
