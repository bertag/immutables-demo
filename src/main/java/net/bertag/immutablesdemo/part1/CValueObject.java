package net.bertag.immutablesdemo.part1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class CValueObject implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private final String id;
  private final String description;
  private final List<Integer> counts;
  
  public CValueObject(String id, String description, List<Integer> counts) {
    this.id = Objects.requireNonNull(id);
    this.description = description;
    this.counts = counts == null ? new ArrayList<>() : counts;
  }
  
  public String getId() {
    return id;
  }
  
  public String getDescription() {
    return description;
  }
  
  public List<Integer> getCounts() {
    return counts;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id, description, counts);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    
    CValueObject other = (CValueObject) obj;
    return Objects.equals(id, other.id)
        && Objects.equals(description, other.description)
        && Objects.equals(counts, other.counts);
  }
  
  @Override
  public String toString() {
    return "ATraditionalBean [id=" + id + ", description=" + description + ", counts=" + counts
        + "]";
  }
  
}
