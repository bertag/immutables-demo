package net.bertag.immutablesdemo.part2;

import java.net.URI;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Parameter;
import org.immutables.value.Value.Style;

@Immutable
@Style(typeAbstract = "Abstract*", typeImmutable = "*")
public abstract class AbstractDefaultsModel {
  
  private static final URI DEFAULT_URI = URI.create("http://localhost/");

  @Parameter
  public abstract String clientId();

  @Default
  public URI rootUri() {
    return DEFAULT_URI;
  };
  
}
