package net.bertag.immutablesdemo.part2;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Parameter;
import org.immutables.value.Value.Style;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Immutable
@Style(typeAbstract = "Abstract*", typeImmutable = "*")
@JsonSerialize(as = JsonModel.class)
@JsonDeserialize(as = JsonModel.class)
public abstract class AbstractJsonModel {
  
  @Parameter
  @JsonProperty
  public abstract String id();
  
  @JsonProperty
  public abstract Optional<String> description();
  
  @Default
  @JsonProperty
  public List<Integer> counts() {
    return Collections.emptyList();
  }
  
}
