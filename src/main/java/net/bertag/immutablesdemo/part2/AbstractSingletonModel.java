package net.bertag.immutablesdemo.part2;

import java.net.URI;
import java.util.Optional;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Redacted;
import org.immutables.value.Value.Style;

@Immutable(singleton = true, builder = false)
@Style(typeAbstract = "Abstract*", typeImmutable = "*", redactedMask = "********")
public abstract class AbstractSingletonModel {
  
  private static final URI DEFAULT_URI = URI.create("http://localhost/");

  @Default
  public URI rootUri() {
    return DEFAULT_URI;
  };
  
  public abstract Optional<String> username();

  @Redacted
  public abstract Optional<String> password();
  
}
