package net.bertag.immutablesdemo.part2;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Lazy;
import org.immutables.value.Value.Parameter;
import org.immutables.value.Value.Style;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Immutable
@Style(typeAbstract = "Abstract*", typeImmutable = "*", strictBuilder = true)
@JsonSerialize(as = LazyModel.class)
@JsonDeserialize(as = LazyModel.class)
public abstract class AbstractLazyModel {
  
  @JsonProperty
  public abstract List<Fulfiller> type1Fulfillers();

  @JsonProperty
  public abstract List<Fulfiller> type2Fulfillers();
  
  @JsonProperty
  public abstract List<Fulfiller> type3Fulfillers();
  
  @Lazy
  @JsonProperty
  public boolean hasActiveFulfiller() {
    return type1Fulfillers().stream().anyMatch(Fulfiller::active)
        || type2Fulfillers().stream().anyMatch(Fulfiller::active)
        || type3Fulfillers().stream().anyMatch(Fulfiller::active);
  }
  
  @Lazy
  public String toString() {
    try {
      return new ObjectMapper().writeValueAsString(this);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
  
  @Immutable
  @Style(typeAbstract = "Abstract*", typeImmutable = "*")
  public abstract static class AbstractFulfiller {

    @Parameter
    public abstract String id();
    
    @Parameter
    public abstract boolean active();
    
  }
  
}
