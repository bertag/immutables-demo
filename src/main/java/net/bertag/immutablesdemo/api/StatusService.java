package net.bertag.immutablesdemo.api;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JAX-RS web service controller that provides at-a-glance information about the current status or
 * health of the application.
 */
@Path("status")
public class StatusService {
  
  private static final Logger logger = LoggerFactory.getLogger(StatusService.class);
  private static String hostName;
  static {
    try {
      hostName = InetAddress.getLocalHost().getCanonicalHostName();
    } catch (UnknownHostException e) {
      logger.error("the local host name could not be resolved into an address", e);
    }
  }
  
  /**
   * Returns a {@code 200 OK} response if this application is currently accepting requests or a
   * non-200 response if is is not currently accepting requests.
   * 
   * @return a JAX-RS response as described
   */
  @GET
  @Path("ping")
  public Response ping() {
    String responseBody = hostName + ": " + ZonedDateTime.now();
    return Response.ok(responseBody).build();
  }
  
  /**
   * Returns a response indicating whether or not this application is currently healthy.
   * 
   * @return a JAX-RS response as described
   */
  @GET
  @Path("health")
  public Response health() {
    return ping();
  }
  
}
