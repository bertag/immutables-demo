package net.bertag.immutablesdemo.part2;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

public class JsonModelTest {
  
  @Test
  public void demo() throws JsonProcessingException {
    JsonModel model = JsonModel.of("abc123")
        .withDescription("some value")
        .withCounts(1, 2, 3, 4, 5);
    
    ObjectMapper mapper = new ObjectMapper();
    System.out.println(mapper.writeValueAsString(model));
    
    ObjectMapper java8mapper = new ObjectMapper()
        .registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
    System.out.println(java8mapper.writeValueAsString(model));
  }
  
}
