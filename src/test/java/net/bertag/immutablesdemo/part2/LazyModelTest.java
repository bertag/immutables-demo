package net.bertag.immutablesdemo.part2;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class LazyModelTest {
  
  @Test
  public void demo() throws JsonProcessingException {
    LazyModel model = LazyModel.builder()
        .addType1Fulfillers(Fulfiller.of("1a", false), Fulfiller.of("1b", false))
        .addType2Fulfillers(Fulfiller.of("2a", true), Fulfiller.of("2b", false))
        .addType3Fulfillers(Fulfiller.of("3a", false), Fulfiller.of("3b", false))
        .build();
    
    System.out.println(model);
  }
  
}
