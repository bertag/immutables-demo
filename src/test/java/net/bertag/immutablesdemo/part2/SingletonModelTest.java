package net.bertag.immutablesdemo.part2;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class SingletonModelTest {
  
  @Test
  public void demo() throws JsonProcessingException {
    SingletonModel model = SingletonModel.of();
    
    System.out.println(model);
    
    model = model
        .withUsername("my user")
        .withPassword("my password");
    
    System.out.println(model);
  }
  
}
