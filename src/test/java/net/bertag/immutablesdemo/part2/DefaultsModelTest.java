package net.bertag.immutablesdemo.part2;

import java.net.URI;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class DefaultsModelTest {
  
  @Test
  public void demo() throws JsonProcessingException {
    DefaultsModel model = DefaultsModel.of("my_client_id");
    
    System.out.println(model);

    model = model.withRootUri(URI.create("https://example.com"));

    System.out.println(model);
  }
  
}
