package net.bertag.immutablesdemo.part1;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import edu.byu.hbll.stats.time.Times;
import edu.byu.hbll.stats.time.Times.Marker;

public class PerformanceComparisonTest2 {
    
    private static int ITERATIONS = 10000;
    
    @Test
    public void spamAList() {
        Marker m = Times.single();

        ATraditionalBean instance = new ATraditionalBean();
        instance.setId("abc123");
        instance.setCounts(new ArrayList<>());
        
        for (int i = 0; i < ITERATIONS; i++) {
            instance.getCounts().add(i);
        }

        m.mark("spamming A list additions");
        System.out.println(m);
    }

    @Test
    public void spamEList() {
        Marker m = Times.single();

        ImmutableEImmutables instance = ImmutableEImmutables.of("abc123");
        
        for (int i = 0; i < ITERATIONS; i++) {
            instance = instance.withCounts(Stream
                    .concat(instance.counts().stream(), Stream.of(i))
                    .collect(Collectors.toList()));
//            instance = instance.withCounts(withNewElement(instance.counts(), i));
        }

        m.mark("spamming E list additions");
        System.out.println(m);
    }
    
//    private <T> List<T> withNewElement(List<T> list, T newElement) {
//        list = new ArrayList<>(list);
//        list.add(newElement);
//        return list;
//    }
    
}
