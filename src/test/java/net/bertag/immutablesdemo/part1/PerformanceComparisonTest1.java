package net.bertag.immutablesdemo.part1;

import java.util.ArrayList;

import org.junit.Test;

import edu.byu.hbll.stats.time.Times;
import edu.byu.hbll.stats.time.Times.Marker;

public class PerformanceComparisonTest1 {
    
    private static int ITERATIONS = 100000000;
    
    @Test
    public void spamADescription() {
        Marker m = Times.single();

        ATraditionalBean instance = new ATraditionalBean();
        instance.setId("abc123");
        instance.setCounts(new ArrayList<>());
        
        for (int i = 0; i < ITERATIONS; i++) {
            instance.setDescription("modification " + i);
        }

        m.mark("spamming A descriptions");
        System.out.println(m);
    }

    @Test
    public void spamEDescription() {
        Marker m = Times.single();

        ImmutableEImmutables instance = ImmutableEImmutables.of("abc123");
        
        for (int i = 0; i < ITERATIONS; i++) {
            instance = instance.withDescription("modification " + i);
        }

        m.mark("spamming E descriptions");
        System.out.println(m);
    }

}
