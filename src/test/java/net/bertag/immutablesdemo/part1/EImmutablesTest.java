package net.bertag.immutablesdemo.part1;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class EImmutablesTest {
    
    @Test
    public void demo() {
        ImmutableEImmutables instance1 = ImmutableEImmutables.builder()
                .id("abc123")
                .description("some non-null description")
                .counts(Arrays.asList(1, 2, 3, 4, 5))
                .build();
        
        EImmutables instance2 = ImmutableEImmutables.of("abc123")
                .withDescription("some non-null description")
                .withCounts(1, 2, 3, 4, 5);
        
        System.out.println(instance1);
        System.out.println(instance2);
        assertEquals(instance1, instance2);
    }
    
}
