package net.bertag.immutablesdemo.part1;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class ATraditionalBeanTest {
    
    @Test
    public void demo() {
        ATraditionalBean instance1 = new ATraditionalBean();
        instance1.setId("abc123");
        instance1.setDescription("some non-null description");
        instance1.setCounts(Arrays.asList(1, 2, 3, 4, 5));

        ATraditionalBean instance2 = new ATraditionalBean();
        instance2.setId("abc123");
        instance2.setDescription("some non-null description");
        instance2.setCounts(Arrays.asList(1, 2, 3, 4, 5));
        
        System.out.println(instance1);
        System.out.println(instance2);
        assertEquals(instance1, instance2);
    }
    
}
